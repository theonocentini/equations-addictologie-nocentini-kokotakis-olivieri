import matplotlib.pyplot as plt
#Variables
nbrIterations = 50
S0 = 0.5 #intensite de self control initiale
C0 = 0 #intensite de fringale initiale
E0 = 1 #intensite d'influence exterieur initiale
q = 0.8 #qte maximale que peut consommer une personne sur un semaine metaboliquement
alpha = 0.2 #effet oubli
b = 0.5 #intensite d'effet d'addiction
p = 0.4 #resilience psychologique
Sm = 0.5 #self-control maximal
h = p*Sm #effet d'addiction bis
k = Sm*p/q #coef de bon
os = 1 # occasions sociales


calculPsi = lambda : C[-1] - S[-1] - E[-1] #etat psychologique
calculV = lambda : min(1, max(Psi[-1], 0)) # vulnerabilite
calculA = lambda : q*V[-1] + os #passage a l'acte

C = [C0]
S = [S0]
E = [E0]
Psi = [calculPsi()]
V = [calculV()]
A = [calculA()]


#Iteration

for k in range(nbrIterations):
    # l'ordre des operations est important
    nextC = C[-1] - alpha*C[-1] + b*min(1, 1-C[-1])*A[-1]
    nextS = S[-1] + p*max(0, Sm - S[-1]) - h*C[-1] - k*A[-1]
    nextE = E[-1]
    C.append(nextC)
    S.append(nextS)
    E.append(nextE)
    Psi.append(calculPsi())
    V.append(calculV())
    A.append(calculA())

semaines = list(range(nbrIterations + 1))

plt.figure("Fringuale et passage à l'acte")
plt.title("Fringuale et passage à l'acte")
plt.plot(semaines, C)
plt.plot(semaines, A)
plt.xlabel('semaines')
plt.legend(['fringuale', "passage à l'acte"])

plt.figure("vulnerabilité vs self-controle")
plt.title("vulnerabilité vs self-controle")
plt.plot(semaines, V)
plt.plot(semaines, S)
plt.xlabel('semaines')
plt.legend(['vunlnerabilité', 'self-control'])
plt.show()


